import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import json from 'rollup-plugin-json'
import copy from 'rollup-plugin-copy-assets'
import { terser } from 'rollup-plugin-terser'

module.exports = [
  {
    input: 'public/js/client.js',
    output: {
      file: 'public/js/client.min.js',
      format: 'iife' // for browsers
    },
    plugins: [terser()]
  },
  {
    input: 'src/server.mjs',
    output: {
      file: 'dist/main.js',
      format: 'cjs', // for Node.jS
      name: 'archives'
    },
    plugins: [
      nodeResolve({
        builtins: false
      }),
      commonjs({
        include: 'node_modules/**'
      }),
      json(),
      copy({
        assets: []
      }),
      terser()
    ],
    external: ['fs', 'http', 'path']
  }
]
